import Vue from 'vue'
import App from './App.vue'
import VXETable from 'vxe-table'
import 'vxe-table/lib/style.css'
import ZmTreeOrg from 'zm-tree-org';
import "zm-tree-org/lib/zm-tree-org.css";

Vue.use(VXETable)
Vue.use(ZmTreeOrg);
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
